<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/forms', function () {
    return view('forms');
});
Route::post('/users/store', 'UserController@store');

Route::resource('/profiles', 'ProfileController');



// Logged In User Endpoints
Route::group(['prefix' => 'users'], function () {
	Route::post('{id}/profile', 'UserController@profile');
	
	Route::get('{id}/clients', 'UserController@clients');
	Route::post('{id}/clients', 'ProfileController@store');
	Route::put('{id}/clients/{profile_id}', 'ProfileController@store');
	Route::delete('{id}/clients/{profile_id}', 'ProfileController@destroy');

	Route::get('{id}/invoices', 'UserController@invoices');
	Route::post('{id}/invoices', 'UserController@invoiceStore');
	Route::put('{id}/clients/{invoice_id}', 'UserController@invoiceStore');
	Route::delete('{id}/clients/{invoice_id}', 'UserController@invoiceDestroy');
	
	Route::get('{id}/sessions/{start_search?}/{weeks?}', 'UserController@sessions');
	Route::post('{id}/sessions', 'UserController@sessionStore');
	Route::put('{id}/sessions', 'UserController@sessionStore');
	Route::delete('{id}/sessions/{session_id}', 'UserController@sessionDestroy');
	
	Route::post('{id}/sessions/{session_id}/clients', 'UserController@sessionClientStore');
	Route::delete('{id}/sessions/{session_id}/clients/{client_id}', 'UserController@sessionClientDestroy');
	
	
	Route::resource('users', 'UserController');
});
