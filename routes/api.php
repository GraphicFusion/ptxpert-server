<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

	
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group(['prefix' => 'users'], function () {
	Route::get('{id}/logout', 'UserController@logout');

	Route::get('{id}/clients', 'UserController@clients');
	Route::post('store', 'UserController@store');
	
	// USER CLIENTS
	Route::get('{id}/clients', 'UserController@clients');
	Route::post('{id}/clients', 'UserController@clientStore');
	Route::put('{id}/clients/{profile_id}', 'UserController@clientStore');
	Route::delete('{id}/clients/{profile_id}', 'UserController@clientDestroy');
	// user's client's sessions
	Route::get('{id}/clients/{client_id}/unpaid-sessions/{start_search?}/{weeks?}', 'UserController@clientUnpaidSessions');
	
	// USER SESSIONS
	Route::get('{id}/sessions/{start_search?}/{weeks?}', 'UserController@sessions');
	Route::post('{id}/sessions', 'UserController@sessionStore');
	Route::put('{id}/sessions/{session_id}', 'UserController@sessionStore');
	Route::delete('{id}/sessions/{session_id}', 'UserController@sessionDestroy');
	
	// CLIENT INVOICES
	Route::get('{id}/invoices', 'UserController@invoices');
	Route::post('{id}/invoices', 'UserController@invoiceStore');
	Route::put('{id}/invoices/{invoice_id}', 'UserController@invoiceStore');
	Route::delete('{id}/invoices/{invoice_id}', 'UserController@invoiceDestroy');

	Route::post('{id}/sessions/{session_id}/clients/{client_id?}', 'UserController@sessionClientStore');
	Route::delete('{id}/sessions/{session_id}/clients/{client_id}', 'UserController@sessionClientDestroy');

	// USER REPORTS
	Route::get('{id}/reports/{start_search?}/{end_search?}', 'UserController@reports');

});
Route::resource('profile', 'ProfileController');
