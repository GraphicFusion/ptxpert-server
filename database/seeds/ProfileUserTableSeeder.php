<?php

use Illuminate\Database\Seeder;

class ProfileUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('profile_user')->insert([
            'profile_id' => 1,
            'user_id' => 1
        ]);
		DB::table('profile_user')->insert([
            'profile_id' => 2,
            'user_id' => 1
        ]);
    }
}
