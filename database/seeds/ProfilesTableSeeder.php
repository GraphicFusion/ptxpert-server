<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('profiles')->insert([
            'first_name' => str_random(10),
            'last_name' => str_random(10)
        ]);
		DB::table('profiles')->insert([
            'first_name' => str_random(10),
            'last_name' => str_random(10)
        ]);
    }
}
