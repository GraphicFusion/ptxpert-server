<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
     		$table->string('first_name')->nullable();
     		$table->string('last_name')->nullable();
     		$table->string('gender')->nullable();
     		$table->tinyInteger('age')->nullable();
     		$table->text('phones')->nullable();
	        $table->text('emails')->nullable();
     		$table->string('photo_path')->nullable();
	        $table->boolean('active')->default(1);
     		$table->boolean('is_trainer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
