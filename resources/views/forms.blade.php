@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
{{  Form::open(array('action'=>'UserController@store', 'method' => 'post')) }}  
	{{ Form::email('email','',$attributes = array('autocomplete'=>'off')) }}
	{{ Form::text('password','',$attributes = array('autocomplete'=>'off')) }}
    {{ Form::submit('Submit') }}
{{ Form::close() }}            

<hr>
<h2>update profile</h2>
{{  Form::open(array('url'=>'users/1/profile', 'method' => 'post', 'files'=>true)) }}  
	{{ Form::label('email', 'E-Mail Address') }}
	{{ Form::email('email','',$attributes = array('autocomplete'=>'off')) }}

	{{ Form::label('first_name', 'First Name') }}
	{{ Form::text('first_name') }}

	{{ Form::label('last_name', 'Last Name') }}
	{{ Form::text('last_name') }}

	{{ Form::label('gender', 'Sex') }}
	{{ Form::select('gender', array('m' => 'He/Him', 'f' => 'She/Her', 't'=>'They/Their')) }}

	{{ Form::label('age', 'Age') }}
	{{ Form::number('age') }}

	{{ Form::label('image', 'Profile Image') }}
	{{ Form::file('image') }}

	{{ Form::label('phones[]', 'Phones') }}
	{{ Form::text('phones[]') }}
	{{ Form::text('phones[]') }}

    {{ Form::submit('Submit') }}
{{ Form::close() }}            
<hr>
<h2>add a client</h2>
{{  Form::open(array('url'=>'users/1/clients', 'method' => 'post', 'files'=>true)) }}  
	{{ Form::label('email', 'E-Mail Address') }}
	{{ Form::email('email','',$attributes = array('autocomplete'=>'off')) }}

	{{ Form::label('first_name', 'First Name') }}
	{{ Form::text('first_name') }}

	{{ Form::label('last_name', 'Last Name') }}
	{{ Form::text('last_name') }}

	{{ Form::label('gender', 'Sex') }}
	{{ Form::select('gender', array('m' => 'He/Him', 'f' => 'She/Her', 't'=>'They/Their')) }}

	{{ Form::label('age', 'Age') }}
	{{ Form::number('age') }}

	{{ Form::label('image', 'Profile Image') }}
	{{ Form::file('image') }}

	{{ Form::label('phones[]', 'Phones') }}
	{{ Form::text('phones[]') }}
	{{ Form::text('phones[]') }}

    {{ Form::submit('Submit') }}
{{ Form::close() }}            


<hr>


{{  Form::open(array('url'=>'users/1/sessions', 'method' => 'post')) }}  
	{{ Form::label('type', 'Type') }}
	{{ Form::number('type') }}

	{{ Form::label('clients[]', 'Client Id') }}
	{{ Form::number('clients[]') }}

	{{ Form::label('start_time', 'Start') }}
	{{ Form::number('start_time') }}

	{{ Form::label('end_time', 'End') }}
	{{ Form::number('end_time') }}

	{{ Form::label('location', 'Location') }}
	{{ Form::text('location') }}

    {{ Form::submit('Submit') }}
{{ Form::close() }}            
<hr>
<h3>Add User to Session</h3>
{{  Form::open(array('url'=>'users/1/clients/18', 'method' => 'delete')) }}  
    {{ Form::submit('Submit') }}
{{ Form::close() }}            
<hr>
<h3>Delete Client from Session</h3>
{{  Form::open(array('url'=>'users/1/sessions/5/clients/2', 'method' => 'delete')) }}  
    {{ Form::submit('Submit') }}
{{ Form::close() }}            
<hr>
<h2>add an invoice</h2>
{{  Form::open(array('url'=>'users/1/invoices', 'method' => 'post', 'files'=>true)) }}  
	

	{{ Form::label('client_id', 'Bill To') }}
	{{ Form::text('client_id') }}

	{{ Form::label('creation_date', 'Date') }}
	{{ Form::text('creation_date') }}
	
	{{ Form::label('description', 'Description') }}
	{{ Form::text('description') }}
	
	{{ Form::label('unit_cost', 'Unit Cost') }}
	{{ Form::text('unit_cost') }}

	{{ Form::label('quantity', 'Quantity') }}
	{{ Form::text('quantity') }}

    {{ Form::submit('Submit') }}
{{ Form::close() }}            

<hr>
{{ Form::open(array('url' => 'users')) }}
    
{{ Form::close() }}            
        </div>
    </div>
</div>
@endsection
