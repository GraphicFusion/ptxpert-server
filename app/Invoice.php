<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

	/**
	* Get the items for the invoice.
	*/
	public function items()
	{
		return $this->hasMany('App\Item');
	}

	/**
	* Get the total for the invoice.
	*/
	public function total()
	{
		$this->total = 0;
		foreach( $this->items as $item ){
			$this->total += $item->unit_cost * $item->quantity;
		}
		return $this->total;
	}

}
