<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
     use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	* Get the clients (profiles) for the user.
	*/
	public function profiles()
	{
		return $this->belongsToMany('App\Profile');
	}

	/**
	* Get the sessions for the user.
	*/
	public function sessions()
	{
		return $this->belongsToMany('App\Session');
	}

	/**
	* Get the invoices for the user.
	*/
	public function invoices()
	{
		return $this->belongsToMany('App\Invoice');
	}

}
