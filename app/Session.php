<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
	/**
	* Get the clients for the session.
	*/
	public function clients()
	{
		return $this->belongsToMany('App\Profile');
	}

	/**
	* Get the trainers for the session.
	*/
	public function users()
	{
		return $this->belongsToMany('App\User');
	}

	/**
     * Get the invoice item that lists the session.
     */
    public function item()
    {
        return $this->hasOne('App\Item');
    }
}
