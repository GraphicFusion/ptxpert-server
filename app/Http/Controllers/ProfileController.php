<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use View;
use \App\Profile;
use Illuminate\Support\Facades\Input;
use \App\Session;
use \App\User;

class ProfileController extends Controller
{
	/**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth:api');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		if( request()->wantsJson() )
		{
			return Profile::all();
		}
		return view("resources.profiles.index",['profiles' => Profile::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user_id, $profile_id = NULL )
    {
		$method = $request->method();
		if ($method == 'POST') {
			$request_name = "user_client";
			$profile = new Profile();
		}
		if ($method == 'PUT') {
			$request_name = "update_user_client";
			$profile = Profile::find($profile_id);
		}
		$data = [];
		$data['request'][$request_name]['emails'] = $request->email;
		$data['request'][$request_name]['first_name'] = $request->first_name;
		$data['request'][$request_name]['last_name'] = $request->last_name;
		$data['request'][$request_name]['gender'] =	$request->gender;
		$data['request'][$request_name]['age'] = ($request->age ? $request->age : NULL);
		$destinationPath = "img/";
		$file = $request->file('image');
		if( !empty($file) ){
			if($file->isValid()){
				$file->move($destinationPath, $file->getClientOriginalName());
				$input = $request->all();
				$input['image']->pathname = $destinationPath.$file->getClientOriginalName();
				$data['request'][$request_name]['photo_path'] = $file->getClientOriginalName();
			}
		}
		$phones = [];
		if( is_array( $request->phones ) ){
			foreach( $request->phones as $phone ){
				if( $phone ){
					$phones[] = $phone;
				}
			}
		}
		$data['request'][$request_name]['phones'] = "";
		if( count($phones) > 0 ){
			$data['request'][$request_name]['phones'] = implode(',',$phones);
		}
		$rules = array(
            'emails' => 'email|max:255',
        );

        $validator = Validator::make(Input::all(), $rules);
        // process the login
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			$user = User::find($user_id);
			$data = checkUser( $user_id , $data );
			if( $data['checkUser'] ){
				// store
				foreach( $data['request'][$request_name] as $key=>$val ){
					$profile->$key = $val;
				}
				$profile->save();
				// user->profiles == user->clients
				$user->profiles()->save($profile);				
				$data['data']['client'] = $profile->getAttributes();
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $user_id, $profile_id)
    {
		$profile = Profile::find($profile_id);
		$data = [];
		$data['request']['delete_profile'] = $profile_id;
		$data = checkUser( $user_id , $data );
		if( $data['checkUser'] ){
			if( is_object( $profile ) ){
				$profile->active = 0;
				$profile->save();
				$data['message'][] = "Client Id ". $profile_id ." has been deleted.";
			}
		}
		return $data;
    }
}
