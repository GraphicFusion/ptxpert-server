<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use \App\Profile;
use \App\Session;
use \App\User;
use \App\Invoice;
use \App\Item;
use \App\oAuthClient;
use Auth;

class UserController extends Controller
{
	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {

		if( request()->wantsJson() )
		{
			$this->middleware('auth:api');
		}
	}


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$email = $request->email;
		$password = $request->password;
		$data = [];
		$rules = array(
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        );
		$data['request']['user_registration']['email'] = $email;
        $validator = Validator::make(Input::all(), $rules);
        // process the login
		$data['success'] = false;
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			// store
			$profile = new Profile();
			$profile->is_trainer = 1;
			$profile->save();

			$user = new User;
			$user->name = $email;
			$user->email = $email;
			$user->profile_id = $profile->id;
			$user->password = bcrypt($password);
			$user->save();

			if( is_object( $user ) ){

				$data['data']['user'] = $user->getAttributes();
				unset($data['data']['user']['password']);

				$profile = Profile::find(1);		
				$user->profiles()->save($profile);
				$profile = Profile::find(2);		
				$user->profiles()->save($profile);
				$data['success'] = true;
//create a dummy client?
			}
        }
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$user = User::find($id);
		$data['request']['user'] = $id;
		$data['success'] = false;
		if( is_object( $user ) ){
			if( $user->profile_id ){
				$data['user'] = $user->getAttributes();
				$profile = Profile::where('id', $user->profile_id )->first();
				$data['user']['profile'] = $profile->getAttributes();
				$data['success'] = true;
				unset($data['user']['password']);
				unset($data['user']['remember_token']);
			}
			$profiles = $user->profiles;
			if( is_object( $profiles ) ){
				foreach( $profiles as $profile ){
					$profile->nextSession;
					$data['user']['clients'][] = $profile->getAttributes();
				}
			}
		}
		else{
			$data['message'][] = "User does not exist.";
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

    /**
     * Logout User - revoke token 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout($id, Request $request)
    {
		$user = User::find($id);
		$data['request']['user'] = $id;
		$data['request']['property'] = 'logout';
		$data['success'] = false;

		// get current auth user and verify id == user->id
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $user ) ){
				$request->user()->token()->revoke();
				$data['success'][] = true;
			}
			else{
				$data['message'][] = "User does not exist.";
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * List all profiles belonging to requested user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clients($id)
    {
		$user = User::find($id);
		$data['request']['user'] = $id;
		$data['request']['property'] = 'clients';
		$data['success'] = false;

		// get current auth user and verify id == user->id
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $user ) ){
				$data['data']['clients'] = [];
				$profiles = $user->profiles->where('active',1);
				if( is_object( $profiles ) ){
					$data['success'] = true;
					if( count( $profiles ) > 0 ){
						foreach( $profiles as $profile ){
							$profile->nextSession;
							$data['data']['clients'][] = $profile->getAttributes();
						}
					}
					else{
						$data['message'][] = "No clients were returned for User.";
					}
				}
				else{
					$data['message'][] = "Issue with requesting clients.";
				}
			}
			else{
				$data['message'][] = "User does not exist.";
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

    /**
     * List all sessions belonging to requested user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sessions($id, $start_search = null, $weeks = null)
    {
		$user = User::find($id);
		$data['request']['user'] = $id;
		$data['request']['property'] = 'sessions';
		$data['success'] = false;
		if( empty($start_search )){
			$start_search = strtotime('last sunday', strtotime('tomorrow'));
		}
		if( empty($weeks ) ){
			$end_search = $start_search + 7*24*60*60;
		}
		else{
			$end_search = $start_search + $weeks*7*24*60*60;
		}
		$data['request']['start_timestamp'] = $start_search;
		$data['request']['end_timestamp'] = $end_search;
		$data['request']['start_of_search'] = date('M d, Y',$start_search);
		$data['request']['end_of_search'] = date('M d, Y',$end_search);

		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $user ) ){
				$data['data']['sessions'] = [];

				$sessions = $user->sessions->where('start_time', '>=', $start_search )->where('start_time', '<', $end_search);
				if( is_object( $sessions ) ){
					$data['success'] = true;
					if( count( $sessions ) > 0 ){
						foreach( $sessions as $session ){
							$session_array = [];
							$clients = $session->clients;
							foreach( $clients as $client){
								$session_array['clients'][] = $client->getAttributes();
							}
							$session_array['id'] = $session->id;
							$session_array['type'] = $session->type;
							$session_array['location'] = $session->location;
							$session_array['invoice_id'] = $session->invoice_id;
							$start_time = $session_array['start_time'] = $session->start_time;
							$end_time = $session_array['end_time'] = $session->end_time;
							$session_array['duration'] = NULL;
							if( $end_time > $start_time ){
								$session_array['duration'] = ($end_time - $start_time)/60;
							}
							$data['data']['sessions'][$start_time][] = $session_array;
						}
						ksort($data['data']['sessions']);
					}
					else{
						$data['message'][] = "No sessions were returned for User.";
					}
				}
				else{
					$data['message'][] = "Issue with requesting sessions.";
				}
			}
			else{
				$data['message'][] = "User does not exist.";
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

    /**
     * List all sessions belonging to requested user's client
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientUnpaidSessions($id, $client_id = Null, $start_search = null, $weeks = null)
    {
		$user = User::find($id);


		$data['request']['user'] = $id;
		$data['request']['client'] = $client_id;
		$data['request']['property'] = 'client unpaid sessions';
		$data['success'] = false;
		if( empty($start_search )){
			$start_search = 0;
		}
		if( empty($weeks ) ){
			$end_search = time();
		}
		else{
			$end_search = $start_search + $weeks*7*24*60*60;
		}
		$data['request']['start_timestamp'] = $start_search;
		$data['request']['end_timestamp'] = $end_search;
		$data['request']['start_of_search'] = date('M d, Y',$start_search);
		$data['request']['end_of_search'] = date('M d, Y',$end_search);

		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $user ) ){
				$data['data']['sessions'] = [];

				// get sessions that belong to trainer, are within date range, that do not belong to invoices (through items) that are paid
				$sessions = Session::with(['users' ,'clients', 'item'])
					->whereHas('users', function($query) use ($id) {
			            $query->where('id', '=', $id);
			        })
					->whereHas('clients', function($query) use ($client_id) {
			            $query->where('id', '=', $client_id);
			        })
					->where( function ($a)  use ($id) {
	 					$a->whereHas('item', function($query) use ($id) {
							$query->whereHas('invoice', function($query) use ($id) {
					            $query->where('paid', '=', 0);
							});
				        })
						->orHas('item', '=', 0);
					})
					->where('start_time', '>=', $start_search)
					->where('start_time', '<', $end_search)
					->get();

				if( is_object( $sessions ) ){
					$data['success'] = true;
					if( count( $sessions ) > 0 ){
						foreach( $sessions as $session ){
							$session_array = [];
							$clients = $session->clients;
							foreach( $clients as $client){
								$session_array['clients'][] = $client->getAttributes();
							}
							$session_array['id'] = $session->id;
							$session_array['type'] = $session->type;
							$session_array['location'] = $session->location;
							$session_array['invoice_id'] = $session->invoice_id;
							$start_time = $session_array['start_time'] = $session->start_time;
							$end_time = $session_array['end_time'] = $session->end_time;
							$session_array['duration'] = NULL;
							if( $end_time > $start_time ){
								$session_array['duration'] = ($end_time - $start_time)/60;
							}
							$data['data']['sessions'][$session->id][] = $session_array;
						}
						ksort($data['data']['sessions']);
					}
					else{
						$data['message'][] = "No sessions were returned for Client.";
					}
				}
				else{
					$data['message'][] = "Issue with requesting sessions.";
				}
			}
			else{
				$data['message'][] = "User does not exist.";
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }


    /**
     * List all invoices belonging to requested user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invoices($id)
    {
		$user = User::find($id);
		$data['request']['user'] = $id;
		$data['request']['property'] = 'invoices';
		$data['success'] = false;

		// get current auth user and verify id == user->id
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $user ) ){
				$data['data']['invoices'] = [];
				$invoices = $user->invoices;
				if( is_object( $invoices ) ){
					$data['success'] = true;
					if( count( $invoices ) > 0 ){
						foreach( $invoices as $invoice ){
							$user_invoice = $invoice->getAttributes();
							$user_invoice['items'] = $invoice->items;
							if( is_object( $user_invoice['items'] )){
								$sessions = [];
								foreach( $user_invoice['items'] as $key=>$item ){
									$session = Session::find($item['session_id']);
									$user_invoice['items'][$key]['session'] = $session;
								}
							}
							$data['data']['invoices'][] = $user_invoice;
						}
					}
					else{
						$data['message'][] = "No invoices were returned for User.";
					}
				}
				else{
					$data['message'][] = "Issue with requesting invoices.";
				}
			}
			else{
				$data['message'][] = "User does not exist.";
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

	 /**
     * List Requested Report for requested user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reports($id, $start_search = Null, $end_search = Null)
    {
		$user = User::find($id);
		$data['request']['user'] = $id;
		$data['request']['property'] = 'report';
		$start_search = date("Y-m-d H:i:s", $start_search);
		if( !$end_search ){
			$end_search = date("Y-m-d H:i:s", time());
		}
		else{
			$end_search = date("Y-m-d H:i:s", $end_search);
		}
		$data['request']['start_search'] = $start_search;
		$data['request']['end_search'] = $end_search;
		$data['success'] = false;

		// get current auth user and verify id == user->id
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $user ) ){
				$data['data']['report'] = [];
				$invoices = $user->invoices;
				$invoices_unpaid = $user->invoices->where('paid','=','0')->where('creation_date','>',$start_search)->where('creation_date','<=',$end_search);
				$invoices_paid = $user->invoices->where('paid','=','1')->where('creation_date','>',$start_search)->where('creation_date','<=',$end_search);
				$data['data']['invoices_unpaid'] = $invoices_unpaid;
				$unpaid_total = 0;
				foreach( $invoices_unpaid as $invoice ){
					$unpaid_total += $invoice->total();
				}
				$paid_total = 0;
				foreach( $invoices_paid as $invoice ){
					$paid_total += $invoice->total();
				}
				$data['data']['unpaid_total'] = $unpaid_total;
				$data['data']['paid_total'] = $paid_total;
				$data['data']['total'] = $unpaid_total + $paid_total;
				$data['data']['invoices_paid'] = $invoices_paid;
				$data['data']['invoices_sent'] = count($invoices_unpaid) + count($invoices_paid);
				$data['data']['invoices'] = [];
				if( is_object( $invoices ) ){
					$data['success'] = true;
					if( count( $invoices ) > 0 ){
						foreach( $invoices as $invoice ){
							$user_invoice = $invoice->getAttributes();
							$user_invoice['items'] = $invoice->items;
							if( is_object( $user_invoice['items'] )){
								$sessions = [];
								foreach( $user_invoice['items'] as $key=>$item ){
									$session = Session::find($item['session_id']);
									$user_invoice['items'][$key]['session'] = $session;
								}
							}
							$data['data']['invoices'][] = $user_invoice;
						}
					}
					else{
						$data['message'][] = "No invoices were returned for User.";
					}
				}
				else{
					$data['message'][] = "Issue with requesting invoices.";
				}
			}
			else{
				$data['message'][] = "User does not exist.";
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }


    /**
     * Store or Update a session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sessionStore(Request $request, $id, $session_id = NULL )
    {
		$method = $request->method();
		if ($method == 'POST') {
			$request_name = "user_session";
			$session = new Session();
		}
		if ($method == 'PUT') {
			$request_name = "update_user_session";
			$session = Session::find($session_id);
		}
		$data = [];
		$data['success'] = false;
		$data['request'][$request_name]['start_time'] = $request->start_time;
		$data['request'][$request_name]['end_time'] = $request->end_time;
		$rules = array(
			'start_time' => 'integer',
			'end_time' => 'integer'
        );
		$data['request'][$request_name]['location'] = $request->location;

        $validator = Validator::make(Input::all(), $rules);
        // process the login
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			$user = User::find($id);
			$data = checkUser( $id , $data );
			if( $data['checkUser'] ){
				// store
				foreach( $data['request'][$request_name] as $key=>$val ){
					$session->$key = $val;
				}
				$session->save();
				$clients = [];
				if( is_array( $request->clients ) ){
					foreach( $request->clients as $client_id ){
						if( $client_id ){
							$client = Profile::find($client_id);
							$session->clients()->save($client);
						}
					}
				}

				$user->sessions()->save($session);				
				$data['success'] = true;
				$data['data']['session'] = $session->getAttributes();
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
	}

    /**
     * Store or Update an invoice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function invoiceStore(Request $request, $user_id, $invoice_id = NULL )
    {
		$user = User::find($user_id);
		$method = $request->method();
		if ($method == 'POST') {
			$request_name = "user_invoice";
			$invoice = new Invoice();
		}
		if ($method == 'PUT') {
			$request_name = "update_user_invoice";
			$invoice = Invoice::find($invoice_id);
		}
		$data = [];
		$rules = array(
        );
		$data['success'] = false;
		$data['request'][$request_name]['client_id'] = $request->client_id;
		if( $invoice->invoice_number ){
			$data['request'][$request_name]['invoice_number'] = $invoice->invoice_number;
		}
		else{
			$counter = $user->invoice_counter;
			$user->invoice_counter = $counter + 1;
			$user->save();
			$data['request'][$request_name]['invoice_number'] = $counter++;
		}
		$data['request'][$request_name]['creation_date'] = $date = date("Y-m-d H:i:s",time());
		$data['request'][$request_name]['items'] = $request->items;
		
        $validator = Validator::make(Input::all(), $rules);
        // process the login
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			$data = checkUser( $user_id , $data );
			if( $data['checkUser'] ){
				// store
				foreach( $data['request'][$request_name] as $key=>$val ){
					if( $key !=  'items' ){
						$invoice->$key = $val;
					}
				}
				$invoice->save();
				if( !empty($request->items) ){
					foreach( $request->items as $item_array ){
						if( array_key_exists ( 'id', $item_array ) && $item_array['id'] ){
							$item = Item::find($item_array['id']);
						}
						else{
							$item = new Item();
						}
						$item->description = $item_array['description'];
						$item->unit_cost = $item_array['unit_cost'];
						$item->quantity = $item_array['quantity'];
						if( array_key_exists('session_id', $item_array) ){
							$item->session_id = $item_array['session_id'];
						}
						$item->invoice_id = $invoice->id;
						$item->save();
					}
				}
				$user->invoices()->save($invoice);
				$data['success'] = true;
				$user_invoice = $invoice->getAttributes();
				$user_invoice['items'] = $invoice->items;
				$data['data']['invoice'] = $user_invoice;
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
	}

   /**
     * Delete an Invoice
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function invoiceDestroy(Request $request, $id, $invoice_id)
    {
		$invoice = Invoice::find($invoice_id);
		$data = [];
		$data['success'] = false;
		$data['request']['delete_invoice'] = $invoice_id;
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $invoice ) ){
				$invoice->delete();
				$data['success'] = true;
				$data['message'][] = "Invoice Id ". $invoice_id ." has been deleted.";
			}
			else{
				$data['message'][] = "Invoice Id ". $invoice_id ." does not exist.";
			}
		}
		return $data;
	}


    /**
     * Delete a Session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sessionDestroy(Request $request, $id, $session_id)
    {
		$session = Session::find($session_id);
		$data = [];
		$data['success'] = false;
		$data['request']['delete_session'] = $session_id;
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $session ) ){
				$session->clients()->detach();	// delete all session clients
				$session->users()->detach();	// delete all session trainers
				$session->delete();
				$data['success'] = true;
				$data['message'][] = "Session Id ". $session_id ." has been deleted.";
			}
			else{
				$data['message'][] = "Session Id ". $session_id ." does not exist.";
			}
		}
		return $data;
	}

    /**
     * Store a new client.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function clientStore(Request $request, $user_id, $profile_id = NULL )
    {
		$method = $request->method();
		if ($method == 'POST') {
			$request_name = "user_client";
			$profile = new Profile();
		}
		if ($method == 'PUT') {
			$request_name = "update_user_client";
			$profile = Profile::find($profile_id);
		}
		$data = [];
		$data['success'] = false;
		$data['request'][$request_name]['emails'] = $request->email;
		$data['request'][$request_name]['first_name'] = $request->first_name;
		$data['request'][$request_name]['last_name'] = $request->last_name;
		$data['request'][$request_name]['gender'] =	$request->gender;
		$data['request'][$request_name]['age'] = ($request->age ? $request->age : NULL);
		$destinationPath = "img/";
		$file = $request->file('image');
		if( !empty($file) ){
			if($file->isValid()){
				$file->move($destinationPath, $file->getClientOriginalName());
				$input = $request->all();
				$input['image']->pathname = $destinationPath.$file->getClientOriginalName();
				$data['request'][$request_name]['photo_path'] = $file->getClientOriginalName();
			}
		}
		$phones = [];
		if( is_array( $request->phones ) ){
			foreach( $request->phones as $phone ){
				if( $phone ){
					$phones[] = $phone;
				}
			}
		}
		$data['request'][$request_name]['phones'] = "";
		if( count($phones) > 0 ){
			$data['request'][$request_name]['phones'] = implode(',',$phones);
		}
		$rules = array(
            'emails' => 'email|max:255',
        );

        $validator = Validator::make(Input::all(), $rules);
        // process the login
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			$user = User::find($user_id);
			$data = checkUser( $user_id , $data );
			if( $data['checkUser'] ){
				// store
				foreach( $data['request'][$request_name] as $key=>$val ){
					$profile->$key = $val;
				}
				$profile->save();
				// user->profiles == user->clients
				if ($method == 'POST') {
					$user->profiles()->save($profile);				
				}
				$data['success'] = true;
				$data['data']['client'] = $profile->getAttributes();
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function clientDestroy(Request $request, $user_id, $profile_id)
    {
		$profile = Profile::find($profile_id);
		$data = [];
		$data['success'] = false;
		$data['request']['delete_profile'] = $profile_id;
		$data = checkUser( $user_id , $data );
		if( $data['checkUser'] ){
			if( is_object( $profile ) ){
				$profile->active = 0;
				$profile->save();
				$data['success'] = true;
				$data['message'][] = "Client Id ". $profile_id ." has been deleted.";
			}
		}
		return $data;
    }

    /**
     * Store or Update a Session's client.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sessionClientStore(Request $request, $id, $session_id = NULL )
    {
		$request_name = "session_client";
		$user = User::find($id);
		$data = [];
		$data['success'] = false;
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			$session = Session::find($session_id);
			if( is_object( $session ) ){
				if( $request->client_id ){
					$profile_id = $request->client_id;
					$profile = Profile::find($profile_id);
					if( !is_object( $profile ) ){
						$data['request']['messages'][] = 'Client not found for client_id '.$profile_id.'.';
					}
					$data['request'][$request_name]['client_id'] = $profile_id;
				}
				else{
					$profile = new Profile();
					$data['request'][$request_name]['emails'] = $request->email;
					$data['request'][$request_name]['first_name'] = $request->first_name;
					$data['request'][$request_name]['last_name'] = $request->last_name;
					$data['request'][$request_name]['gender'] =	$request->gender;
					$data['request'][$request_name]['age'] = ($request->age ? $request->age : NULL);
					$destinationPath = "img/";
					$file = $request->file('image');
					if( !empty($file) ){
						if($file->isValid()){
							$file->move($destinationPath, $file->getClientOriginalName());
							$input = $request->all();
							$input['image']->pathname = $destinationPath.$file->getClientOriginalName();
							$data['request'][$request_name]['photo_path'] = $file->getClientOriginalName();
						}
					}
					$phones = [];
					if( is_array( $request->phones ) ){
						foreach( $request->phones as $phone ){
							if( $phone ){
								$phones[] = $phone;
							}
						}
					}
					$data['request'][$request_name]['phones'] = "";
					if( count($phones) > 0 ){
						$data['request'][$request_name]['phones'] = implode(',',$phones);
					}
					$rules = array(
				        'emails' => 'email|max:255',
				    );
				
				    $validator = Validator::make(Input::all(), $rules);
				    // process the login
				    if ($validator->fails()) {
						$messages = $validator->messages();
						foreach ($messages->all() as $message)
						{
							$data['request']['validation_messages'][] = $message;
						}
				    }
					else {
						// store
						foreach( $data['request'][$request_name] as $key=>$val ){
							$profile->$key = $val;
						}
	
						$profile->save();
						$user->profiles()->save($profile);				
						$data['data']['client'] = $profile->getAttributes();
					}
				}
				$data['success'] = true;
				$session->clients()->save($profile);				
				$data['data']['session'] = $session->getAttributes();
				$data['data']['session']['clients'] = $session->clients();
			}
			else{
				$data['request']['messages'][] = 'Session not found for session_id '.$session_id.'.';
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
	}

    /**
     * Delete a Client from a Session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sessionClientDestroy(Request $request, $id, $session_id, $client_id)
    {
		$session = Session::find($session_id);
		$client = Profile::find($client_id);
		$data = [];
		$data['success'] = false;
		$data['request']['sessionClientDestroy']['session_id'] = $session_id;
		$data['request']['sessionClientDestroy']['client_id'] = $client_id;
		$data = checkUser( $id , $data );
		if( $data['checkUser'] ){
			if( is_object( $session ) && is_object($client)){
				$session->clients()->detach($client_id);
				$data['success'] = true;
				$data['message'][] = "Client ID ".$client_id." of Session Id ". $session_id ." has been deleted.";
			}
			else{
				$data['message'][] = "";
				if( !is_object( $session ) ){
					$data['message'][] = " Session Id ". $session_id ." does not exist.";
				}
				if(!is_object($client)){
					$data['message'] .= " Client ID ".$client_id." does not exist.";
				}
			}
		}
		return $data;
	}

    /**
     * Save a User's Profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request, $user_id, $profile_id = NULL)
    {
		$method = $request->method();
		if ($method == 'POST') {
			$request_name = "user_client";
			$profile = new Profile();
		}
		if ($method == 'PUT') {
			$request_name = "update_user_client";
			$profile = Profile::find($profile_id);
		}
		$data = [];
		$data['success'] = false;
		$data['request'][$request_name]['emails'] = $request->email;
		$data['request'][$request_name]['first_name'] = $request->first_name;
		$data['request'][$request_name]['last_name'] = $request->last_name;
		$data['request'][$request_name]['gender'] =	$request->gender;
		$data['request'][$request_name]['age'] = ($request->age ? $request->age : NULL);
		$destinationPath = "img/";
		$file = $request->file('image');
		if($file->isValid()){
			$file->move($destinationPath, $file->getClientOriginalName());
			$input = $request->all();
			$input['image']->pathname = $destinationPath.$file->getClientOriginalName();
			$data['request'][$request_name]['photo_path'] = $file->getClientOriginalName();
		}
		$phones = [];
		if( is_array( $request->phones ) ){
			foreach( $request->phones as $phone ){
				if( $phone ){
					$phones[] = $phone;
				}
			}
		}
		$data['request'][$request_name]['phones'] = "";
		if( count($phones) > 0 ){
			$data['request'][$request_name]['phones'] = implode(',',$phones);
		}
		$rules = array(
            'emails' => 'email|max:255',
        );

        $validator = Validator::make(Input::all(), $rules);
        // process the login
        if ($validator->fails()) {
			$messages = $validator->messages();
			foreach ($messages->all() as $message)
			{
				$data['request']['validation_messages'][] = $message;
			}
        }
		else {
			$user = User::find($user_id);
			$data = checkUser( $user_id , $data );
			if( $data['checkUser'] ){
				// store
				foreach( $data['request'][$request_name] as $key=>$val ){
					$profile->$key = $val;
				}
				$profile->save();
				// user->profiles == user->clients
				$user->profile_id = $profile->id;
				$user->save();
				$data['success'] = true;
				$data['data']['client'] = $profile->getAttributes();
			}
		}
		if( request()->wantsJson() )
		{
			return $data;
		}
		return view("generic",['data' => $data]);
	}
}
