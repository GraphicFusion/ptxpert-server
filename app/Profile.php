<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $appends = array('nextSession');

    public function getnextSessionAttribute()
    {
		$this->nextSession = "";
		$now = strtotime('last sunday', strtotime('tomorrow'));
		$session = $this->sessions->sortBy('start_time')->where('start_time','>',$now)->first();
		if( is_object( $session ) ){
			$this->nextSession = $session->getAttributes();
		}
    }

	/**
	* Get the sessions for the client.
	*/
	public function sessions()
	{
		return $this->belongsToMany('App\Session');
	}

	/**
	* Get the next session for the client.
	*/
	public function next_session()
	{
		$sessions = $this->belongsToMany('App\Session');
		foreach( $sessions as $session ){

		}
	}


}
