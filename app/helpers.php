<?php
	use Illuminate\Support\Facades\Auth as Auth;
	function checkUser( $id, $data ){
		$current_user = "";
		if( Auth::check() ){
			$current_user = Auth::user();
			$data['authenticated_user'] = $current_user->getAttributes();
			unset($data['authenticated_user']['password']);
		}
		if( $current_user ){
			if( $current_user->id != $id ){
				$data['checkUser'] = false;
				$data['message'] = "Authenticated User does not have permission to view Requested User.";
			}
			else{
				$data['checkUser'] = true;
			}
		}
		else{
			$data['checkUser'] = false;
			$data['message'] = "User is not logged in.";
		}
		return $data;
	}
?>