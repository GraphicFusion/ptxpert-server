<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * Get the session record associated with the item.
     */
    public function session()
    {
        return $this->belongsTo('App\Session');
    }

    /**
     * Get the session record associated with the item.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
}
